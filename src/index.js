import React from "react";
import ReactDOM from "react-dom";
import "normalize.css";
import { IntlProvider } from "react-intl";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import Site from "./Site";

ReactDOM.render(
  <IntlProvider locale="ru">
    <Site />
  </IntlProvider>,
  document.getElementById("site")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
