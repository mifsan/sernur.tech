const commonTheme = {
  fonts: {
    subtitle: {
      fontFamily: "Futura PT Book",
      opacity: 0.4,
      fontSize: "17px",
      textTransform: "uppercase"
    },
    heading: {
      fontFamily: "Futura PT Bold",
      opacity: 0.87,
      fontSize: "54px",
      textTransform: "uppercase",
      letterSpacing: ".2em"
    },
    text: {
      fontFamily: "Futura PT Light",
      opacity: 0.87,
      fontSize: "20px"
    },
    caseHeading: {
      fontFamily: "Futura PT Light",
      opacity: 0.87,
      fontSize: "17px",
      textTransform: "uppercase",
      letterSpacing: ".3em"
    }
  }
};

export const blackTheme = {
  ...commonTheme,
  fonts: { ...commonTheme.fonts, color: "white" }
};
