import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import logo from "../../resources/img/logo.svg";

const StyledA = styled.a`
  display: flex;
  flex: 3;
  color: white;
  &:visited {
    color: white;
  }
`;

const StyledImg = styled.img`
  width: ${props => props.width};
  margin-left: ${props => props.marginLeft};
`;

const Logo = ({ width, marginLeft }) => {
  return (
    <StyledA href="/">
      <StyledImg src={logo} marginLeft={marginLeft} width={width} alt="logo" />
    </StyledA>
  );
};

Logo.propTypes = {
  width: PropTypes.string,
  marginLeft: PropTypes.string
};

export default Logo;
