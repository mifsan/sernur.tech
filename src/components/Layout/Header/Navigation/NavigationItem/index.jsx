import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const StyledLi = styled.li`
  display: inline-flex;
  margin-left: 20px;
  &:first-child {
    margin-left: 0px;
  }
`;

const StyledA = styled.a`
  font-family: ${props => props.theme.fonts.text.fontFamily};
  font-size: ${props => props.theme.fonts.text.fontSize};
  text-decoration: none;
  color: ${props => props.theme.fonts.color};
  opacity: ${props => props.theme.fonts.text.opacity};
  &:visited {
    color: white;
  }
  &:active {
    color: #898989;
  }
`;

const NavigationItem = ({ text, anchor }) => {
  return (
    <StyledLi>
      <StyledA href={anchor}>{text}</StyledA>
    </StyledLi>
  );
};

NavigationItem.propTypes = {
  text: PropTypes.string,
  anchor: PropTypes.string
};

export default NavigationItem;
