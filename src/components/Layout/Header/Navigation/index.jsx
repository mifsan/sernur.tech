import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import NavigationItem from "./NavigationItem";

const StyledNav = styled.nav`
  display: flex;
  flex: 1;
`;

const StyledUl = styled.ul`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
`;

const Navigation = ({ navigationItems }) => {
  return (
    <StyledNav>
      <StyledUl>
        {navigationItems.map(el => (
          <NavigationItem key={el.anchor} text={el.text} anchor={el.anchor} />
        ))}
      </StyledUl>
    </StyledNav>
  );
};

Navigation.propTypes = {
  navigationItems: PropTypes.arrayOf(
    PropTypes.shape({ text: PropTypes.string, anchor: PropTypes.string })
  ).isRequired
};

export default Navigation;
