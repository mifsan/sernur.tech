import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Navigation from "./Navigation";

const StyledNavigationHeader = styled.header`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
`;

const StyledSection = styled.section`
  display: flex;
  flex-direction: column;
`;

const Header = ({ navigationItems, logo }) => {
  return (
    <StyledSection>
      <StyledNavigationHeader>
        {logo}
        <Navigation navigationItems={navigationItems} />
      </StyledNavigationHeader>
    </StyledSection>
  );
};

Header.propTypes = {
  navigationItems: PropTypes.arrayOf(
    PropTypes.shape({ text: PropTypes.string, anchor: PropTypes.string })
  ).isRequired,
  logo: PropTypes.element.isRequired
};

export default Header;
