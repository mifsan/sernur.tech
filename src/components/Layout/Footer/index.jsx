import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const StyledFooter = styled.footer`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  flex-direction: row;
  padding-bottom: 9px;
`;

const StyledText = styled.p`
  font-family: ${props => props.theme.fonts.text.fontFamily};
  font-size: ${props => props.theme.fonts.text.fontSize};
  opacity: ${props => props.theme.fonts.text.opacity};
  text-align: ${props => props.textAlign};
  color: ${props => props.theme.fonts.color};
  margin: 0;
  line-height: 26px;
`;

const StyledHyperlink = styled.a`
  font-family: ${props => props.theme.fonts.text.fontFamily};
  font-size: ${props => props.theme.fonts.text.fontSize};
  opacity: ${props => props.theme.fonts.text.opacity};
  text-align: ${props => props.textAlign};
  color: ${props => props.theme.fonts.color};
  margin: 0;
  line-height: 26px;
  text-decoration: none;
  &:visited {
    color: white;
  }
  &:active {
    color: #898989;
  }
`;

const StyledSection = styled.section`
  display: flex;
  justify-content: ${props => props.justify};
  flex-direction: column;
`;

const Footer = ({ sections }) => {
  return (
    <StyledFooter>
      <StyledSection justify="flex-start">
        {sections.hashtags.map((hashtag, index) => (
          <StyledText textAlign="left" key={index}>
            {hashtag}
          </StyledText>
        ))}
      </StyledSection>
      <StyledSection justify="center">
        {sections.copyrights.map((copyright, index) => (
          <StyledText textAlign="center" key={index}>
            {copyright}
          </StyledText>
        ))}
      </StyledSection>
      <StyledSection justify="flex-end">
        {sections.sites.map((site, index) => (
          <StyledHyperlink
            href={"http://" + site}
            textAlign="right"
            key={index}
          >
            {site}
          </StyledHyperlink>
        ))}
      </StyledSection>
    </StyledFooter>
  );
};

Footer.propTypes = {
  sections: {
    hashtags: PropTypes.arrayOf(PropTypes.string).isRequired,
    copyrights: PropTypes.arrayOf(PropTypes.string).isRequired,
    sites: PropTypes.arrayOf(PropTypes.sites).isRequired
  }
};

export default Footer;
