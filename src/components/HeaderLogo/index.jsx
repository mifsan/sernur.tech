import React from "react";
import styled from "styled-components";
import logo from "../../resources/img/logo.svg";

const StyledSection = styled.section`
  display: flex;
  flex: 1;
  padding-top: 127px;
  padding-bottom: 127px;
  justify-content: space-between;
`;

const StyledDiv = styled.div`
  display: flex;
  flex-direction: ${props => props.direction};
  align-items: ${props => props.align};
  justify-content: ${props => props.justify};
`;

const StyledSubtitle = styled.h2`
  font-family: ${props => props.theme.fonts.subtitle.fontFamily};
  font-size: ${props => props.theme.fonts.subtitle.fontSize};
  opacity: ${props => props.theme.fonts.subtitle.opacity};
  color: ${props => props.theme.fonts.color};
  text-transform: ${props => props.theme.fonts.subtitle.textTransform};
  margin: ${props => props.margin || 0};
  position: absolute;
`;
/*const StyledLine = styled.span`
  width: 65px;
  opacity: ${props => props.theme.fonts.text.opacity};
  border: 1px solid ${props => props.theme.fonts.color};
`;

const StyledNumDecoration = styled.p``;*/

const StyledHeading = styled.h1`
  margin: 0;
`;

const HeadingLogo = () => {
  return (
    <StyledSection>
      <StyledDiv></StyledDiv>
      <StyledDiv align="center" direction="column">
        <StyledSubtitle margin="41px 0 0 -103px">We are</StyledSubtitle>
        <StyledHeading>
          <img src={logo} alt="Sernur.tech" />
        </StyledHeading>
        <StyledSubtitle margin="197px 0 0 81px">IT agency</StyledSubtitle>
      </StyledDiv>
      <StyledDiv align="space-between" justify="space-between"></StyledDiv>
    </StyledSection>
  );
};

HeadingLogo.propTypes = {};

export default HeadingLogo;
